# Kittens API

Una vez levantado el container de docker, la API escucha a través del puerto 8383.
Ejemplos:
```
$ curl localhost:8383/api/cat -H "Authorization: Bearer [](){}"

GET /api/cat HTTP/1.1" 200
{"url": "https://cdn2.thecatapi.com/images/b99.jpg"}
```

```
$ curl localhost:8383/api/cat -H "Authorization: Bearer [](({)})"

GET /api/cat HTTP/1.1" 401
{"detail":"The token is incorrect"}
```
