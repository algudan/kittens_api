import httpx
import json


async def get_kitten_image_link():
    url = 'https://api.thecatapi.com/v1/images/search'
    async with httpx.AsyncClient() as client:
        try:
            response: httpx.Response = await client.get(url)
            response.raise_for_status()
        except httpx.HTTPError:
            return False

        if not response:
            return False

        json_response = response.json()
        if not json_response or 'url' not in json_response[0]:
            return False

        return json_response[0]['url']


