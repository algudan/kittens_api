from fastapi import FastAPI, Depends, HTTPException, status, Request
from fastapi.security.base import SecurityBase
from fastapi.security.utils import get_authorization_scheme_param
from fastapi.responses import RedirectResponse, JSONResponse
from fastapi.middleware.cors import CORSMiddleware
import queue
import app.kitten_data as kitten_data


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=['*'],
    allow_headers=['*'],
    allow_credentials=True,
)


class BasicAuth(SecurityBase):

    async def __call__(self, request: Request):
        basic_auth_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Could not validate token',
            headers={'WWW-Authenticate': 'Bearer'}
        )

        authorization: str = request.headers.get('Authorization')
        if not authorization:
            raise basic_auth_exception

        scheme, token = get_authorization_scheme_param(authorization)
        if scheme != 'Bearer':
            raise basic_auth_exception

        basic_auth_exception.detail = 'The token is incorrect'
        open_chars = [ '[', '(', '{' ]
        close_chars = [ ']', ')', '}' ]
        q = queue.LifoQueue()
        for char in token:
            if char in open_chars:
                q.put(char)
                continue

            try:
                position = close_chars.index(char)
            except ValueError:
                raise basic_auth_exception

            try:
                item = q.get(block=False)
            except queue.Empty:
                raise basic_auth_exception
            if item != open_chars[position]:
                raise basic_auth_exception

        if not q.empty():
            raise basic_auth_exception


basic_auth = BasicAuth()


@app.get('/')
def index():
    return {
        "message": "Hello world!",
        "usage": "Call /api/cat to use the API"
    }


@app.get('/api/cat')
async def get_random_cat(auth: BasicAuth = Depends(basic_auth)):
    kitten_url = await kitten_data.get_kitten_image_link()
    if not kitten_url:
        raise HTTPException(
            status_code=503,
            detail='Kittens provider is shutdown',
        )

    return JSONResponse(content={"url": kitten_url})
