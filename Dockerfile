FROM python:3.8-slim-buster

WORKDIR /app

RUN pip3 install gunicorn uvloop httptools

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

ENV ACCESS_LOG=${ACCESS_LOG:-/proc/1/fd/1}
ENV ERROR_LOG=${ERROR_LOG:-/proc/1/fd/2}

ENTRYPOINT /usr/local/bin/gunicorn \
	-b 0.0.0.0:80 \
	-w 4 \
	-k uvicorn.workers.UvicornWorker \
	app.main:app \
	--access-logfile "$ACCESS_LOG" \
	--error-logfile "$ERROR_LOG" \
	--chdir /app
